/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.fileinputoutput;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class RSTGameFrame extends javax.swing.JFrame {

    /**
     * Creates new form RSTGameFrame
     */
    public RSTGameFrame() {
        initComponents();
        initRSPButton();
        computer = new Computer();
        load();
        showStat();
    }

    public void initRSPButton() {
        ImageIcon imgRock = new ImageIcon("img/Rock.png");
        btnRock.setIcon(imgRock);
        ImageIcon imgScissors = new ImageIcon("img/Scissors.png");
        btnScissor.setIcon(imgScissors);
        ImageIcon imgPaper= new ImageIcon("img/Paper.png");
        btnPaper.setIcon(imgPaper);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblStat = new javax.swing.JLabel();
        lblCom = new javax.swing.JLabel();
        lblPlayer = new javax.swing.JLabel();
        lblCom2 = new javax.swing.JLabel();
        lblPlayer2 = new javax.swing.JLabel();
        btnPaper = new javax.swing.JButton();
        btnScissor = new javax.swing.JButton();
        btnRock = new javax.swing.JButton();
        lblResult = new javax.swing.JLabel();
        btnReset = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        lblStat.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblStat.setText("Win: 0 Draw: 0 Lose: 0");

        lblCom.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblCom.setText("Com");

        lblPlayer.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPlayer.setText("Player");

        lblCom2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblCom2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        lblPlayer2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblPlayer2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        btnPaper.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnPaper.setText("Paper");
        btnPaper.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPaperActionPerformed(evt);
            }
        });

        btnScissor.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnScissor.setText("Scissor");
        btnScissor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnScissorActionPerformed(evt);
            }
        });

        btnRock.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnRock.setText("Rock");
        btnRock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRockActionPerformed(evt);
            }
        });

        lblResult.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblResult.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblResult.setText("Resulit:");

        btnReset.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        btnReset.setText("RESET");
        btnReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResetActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblResult, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(lblCom2, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblPlayer2, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(33, 33, 33))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblCom, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblPlayer, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnReset, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnRock, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnScissor, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnPaper, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 14, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(lblStat, javax.swing.GroupLayout.PREFERRED_SIZE, 305, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblStat)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCom)
                    .addComponent(lblPlayer))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPlayer2, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCom2, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addComponent(lblResult)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRock, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnScissor, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPaper, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(btnReset, javax.swing.GroupLayout.DEFAULT_SIZE, 61, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnScissorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnScissorActionPerformed
        int status = computer.paoYinShub(1);
        process();
    }//GEN-LAST:event_btnScissorActionPerformed

    private void process() {
        showComputer();
        showPlayer();
        showResult();
        showStat();
        save();
    }

    private void btnPaperActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPaperActionPerformed
        int status = computer.paoYinShub(2);
        process();
    }//GEN-LAST:event_btnPaperActionPerformed

    private void btnRockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRockActionPerformed
        int status = computer.paoYinShub(0);
        process();
    }//GEN-LAST:event_btnRockActionPerformed

    private void btnResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResetActionPerformed
        computer = new Computer();
        save();
        showStat();
    }//GEN-LAST:event_btnResetActionPerformed
    private String[] strRSPs = {"ROCK","SCISSORS","PAPER"};
    private void showPlayer(){
       // lblPlayer2.setText(strRSPs[computer.getPlayerHand()]);
        ImageIcon imgRock = new ImageIcon("img/" + strRSPs[computer.getPlayerHand()] + ".png");
        lblPlayer2.setIcon(imgRock);
    }
    private void showComputer(){
        //lblCom2.setText(strRSPs[computer.getHand()]);
        ImageIcon imgRock = new ImageIcon("img/"+strRSPs[computer.getHand()]+".png");
        lblCom2.setIcon(imgRock);
    }
    private void showResult(){
        if(computer.getStatus()==0){
            lblResult.setText("Draw!!!");
        }else if(computer.getStatus()==1){
            lblResult.setText("Win!!!");
        }else if(computer.getStatus()==-1){
            lblResult.setText("Lose!!!");
        }
    }
    private void showStat() {
        lblStat.setText("Win: " + computer.getWin()
                + " Draw: " + computer.getDraw()
                + " Lose: " + computer.getLose());
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RSTGameFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RSTGameFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RSTGameFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RSTGameFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RSTGameFrame().setVisible(true);
            }
        });
    }private void save(){
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            File file = new File("computer.obj");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(computer);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            System.out.println("File not found!!!");
        } catch (IOException ex) {
            Logger.getLogger(RSTGameFrame.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(RSTGameFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    private void load(){
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            File file = new File("computer.obj");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            computer = (Computer)ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            System.out.println("File not found!!!");
        } catch (IOException ex) {
            Logger.getLogger(RSTGameFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(RSTGameFrame.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if(fis!=null)
                fis.close();
            } catch (IOException ex) {
                Logger.getLogger(RSTGameFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        }
    
    private Computer computer;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnPaper;
    private javax.swing.JButton btnReset;
    private javax.swing.JButton btnRock;
    private javax.swing.JButton btnScissor;
    private javax.swing.JLabel lblCom;
    private javax.swing.JLabel lblCom2;
    private javax.swing.JLabel lblPlayer;
    private javax.swing.JLabel lblPlayer2;
    private javax.swing.JLabel lblResult;
    private javax.swing.JLabel lblStat;
    // End of variables declaration//GEN-END:variables
}
